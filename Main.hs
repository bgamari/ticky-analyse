{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Data.Word
import Data.Ord
import Data.List (sortBy)
import GHC.RTS.Events
import qualified Data.Map.Strict as M
import qualified Data.Text as T

data Counter
  = Counter { cArity       :: !Word16
            , cArgKinds    :: !T.Text
            , cCounterName :: !T.Text
            }
  deriving (Show)

data CounterSample
  = CounterSample { csCounter :: !Counter
                  , csEntryCount :: !Word64
                  , csAllocs :: !Word64
                  , csAllocd :: !Word64
                  }
  deriving (Show)

collectTickyCounters :: FilePath -> IO (M.Map Word64 Counter)
collectTickyCounters fname = do
    Right elog <- readEventLogFromFile fname
    return $ M.fromList
      [ (tickyCtrDefId, Counter tickyCtrDefArity tickyCtrDefKinds tickyCtrDefName)
      | Event { evSpec = TickyCounterDef {..} } <- events $ dat elog
      ]

collectTickySamples :: (Event -> Maybe a) 
                    -> FilePath
                    -> IO [(Timestamp, Either a [CounterSample])]
collectTickySamples f fname = do
    counters <- collectTickyCounters fname
    Right elog <- readEventLogFromFile fname
    return $ collectTickySamples' f counters (sortEvents $ events $ dat elog)

collectTickySamples' :: forall a. ()
                     => (Event -> Maybe a)
                     -> M.Map Word64 Counter 
                     -> [Event]
                     -> [(Timestamp, Either a [CounterSample])]
collectTickySamples' f counters =
    go 0 []
  where
    go :: Timestamp -> [CounterSample] 
       -> [Event]
       -> [(Timestamp, Either a [CounterSample])]
    go t0 accum [] = [(t0, Right accum)]
    go t0 accum (Event { evTime = t0', evSpec = TickyBeginSample} : rest) =
      let rest' = go t0' [] rest
      in if null accum
          then rest'
          else (t0, Right accum) : rest'
    go t0 accum (Event { evSpec = TickyCounterSample {..} } : rest) =
      let s = CounterSample { csCounter = counters M.! tickyCtrSampleId
                            , csEntryCount = tickyCtrSampleEntryCount
                            , csAllocs = tickyCtrSampleAllocs
                            , csAllocd = tickyCtrSampleAllocd
                            }
      in go t0 (s:accum) rest
    go t0 accum (ev : rest)
      | Just x <- f ev = (evTime ev, Left x) : go t0 accum rest
      | otherwise      = go t0 accum rest

isMarkerLike :: Event -> Maybe T.Text
isMarkerLike (Event { evSpec = Message msg })     = Just msg
isMarkerLike (Event { evSpec = UserMarker msg })  = Just msg
isMarkerLike (Event { evSpec = UserMessage msg }) = Just msg
isMarkerLike _ = Nothing

timeToSeconds :: Timestamp -> Double
timeToSeconds t = realToFrac t / 1e9

main :: IO ()
main = do
    let fname = "ghc.eventlog"

    let f ev
          | Just msg <- isMarkerLike ev
          , "GHC:" `T.isPrefixOf` msg = Just msg
          | otherwise = Nothing

    samples <- collectTickySamples f fname
    putStrLn $ unlines $ map showSample samples

showSample :: (Timestamp, Either T.Text [CounterSample]) -> String
showSample (t, Left msg) = show (timeToSeconds t) ++ ": Marker: " ++ T.unpack msg
showSample (t, Right counters) =
    unlines $
    [ show (timeToSeconds t) ++ ": Ticky sample:" ]
    ++ take 10
       [ "  " ++ show (csAllocs c) ++ "    " ++ T.unpack (cCounterName $ csCounter c)
       | c <- sortBy (flip $ comparing csAllocs) counters
       ]
  
